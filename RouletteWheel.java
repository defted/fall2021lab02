import java.util.Random;
public class RouletteWheel {
    private Random rand;
    private int lastSpin = 0;

    public RouletteWheel() {
        rand = new Random();
    }

    public void spin() {
        this.lastSpin = this.rand.nextInt(27);
        System.out.println("Your number is: " + this.lastSpin);
    }
    
    public int getValue() {
        return this.lastSpin;
    }

}