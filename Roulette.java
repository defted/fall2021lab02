import java.util.Scanner;
public class Roulette {
    public static void main (String[] args) {
        // Creating Roulette Object
        RouletteWheel game = new RouletteWheel();

        // Asking for bet
        Scanner scan = new Scanner(System.in);
        String answer = scan.nextLine();
        System.out.println("Would you like to play roulette? (Yes/No) ");
        if (answer == "No" && answer != "Yes") {
            System.out.println("You shall not play.");
        }
            int num = scan.nextInt();
            System.out.println("What number would you like to bet on: ");

        // Spinning the game
        game.spin();

        // Check who won
        int value = game.getValue();
        if (num == value) {
            System.out.println("You win!");
            System.out.println("Your number: " +num);
            System.out.println("Roulette number: " +value);
        } else {
            System.out.println("You lose!");
            System.out.println("Your number: " +num);
            System.out.println("Roulette number: " +value);
        }
        System.out.println("Goodbye.");
        }
    }